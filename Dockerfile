FROM openjdk:8-jdk-alpine
LABEL maintainer="lseverino@gmail.com"
COPY ./build/libs/sicredi-test-0.0.1.jar /usr/app/
WORKDIR /usr/app
EXPOSE 8080
RUN sh -c 'touch sicredi-test-0.0.1.jar'
ENTRYPOINT ["java","-jar","sicredi-test-0.0.1.jar"]

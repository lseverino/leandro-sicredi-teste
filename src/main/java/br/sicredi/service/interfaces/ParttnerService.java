package br.sicredi.service.interfaces;

import br.com.sicredi.entity.dto.KeyDTO;

public interface ParttnerService {

	KeyDTO checkPartner(String cpf);

}
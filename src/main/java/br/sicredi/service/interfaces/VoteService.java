package br.sicredi.service.interfaces;

import br.com.sicredi.entity.dto.KeyDTO;
import br.com.sicredi.entity.dto.VoteDTO;

public interface VoteService {

	KeyDTO createVote(Long discussionId, VoteDTO voteDto);

}
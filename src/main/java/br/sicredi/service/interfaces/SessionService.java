package br.sicredi.service.interfaces;

import br.com.sicredi.entity.dto.KeyDTO;
import br.com.sicredi.entity.dto.SessionDTO;

public interface SessionService {

	KeyDTO createSession(Long discussionId, SessionDTO sessionDto);

	SessionDTO findByDiscussionId(Long discussionId);

	SessionDTO findSessionSummaryByDiscussionId(Long discussionId);

}
package br.com.sicredi.util;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
@Constraint(validatedBy = NonPastDateValidator.class)
public @interface NonPastDate {

	String message() default "Date can not be before present date;";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};


}

package br.com.sicredi.api;

import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sicredi.client.ResponseOutput;
import br.com.sicredi.client.VoteIn;
import br.com.sicredi.entity.dto.KeyDTO;
import br.com.sicredi.entity.dto.VoteDTO;

@RestController
@RequestMapping("/v1/discussions")
public class SessionVoteApi extends ApiBase {

	private static final long serialVersionUID = -112893215845287402L;

	@Autowired
	private ModelMapper modelMapper;

	@ApiOperation(value = "Criação de votos para sessão.")
	@PostMapping(path = "/{discussionId}/votes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createVote(@PathVariable Long discussionId, @RequestBody @Valid VoteIn vote, BindingResult result) {
		logger.info(StringUtils.join("createVote method ", discussionId));

		if (result.hasErrors()) {
			return errorsResponse(result);
		}

		final ResponseOutput<KeyDTO> response = new ResponseOutput<KeyDTO>(voteService.createVote(discussionId, modelMapper.map(vote, VoteDTO.class)));

		logger.info(StringUtils.join("createVote method ", response));

		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
}

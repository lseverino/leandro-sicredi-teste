package br.com.sicredi.api;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.io.Serializable;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.sicredi.client.ResponseOutput;
import br.com.sicredi.util.BusinessException;
import br.sicredi.service.interfaces.SessionService;
import br.sicredi.service.interfaces.VoteService;

@ApiResponses(value = {
		@ApiResponse(code = 200, message = "OK.", response = ResponseOutput.class),
		@ApiResponse(code = 201, message = "Created.", response = ResponseOutput.class),
		@ApiResponse(code = 403, message = "Forbidden", response = ResponseOutput.class),
		@ApiResponse(code = 404, message = "Not Found", response = ResponseOutput.class),
		@ApiResponse(code = 500, message = "Internal Server Error", response = ResponseOutput.class) })
public abstract class ApiBase implements Serializable {

	private static final long serialVersionUID = -8060759718288118627L;

	@Autowired
	public Logger logger;
	
	@Autowired
	public SessionService sessionService;
	
	@Autowired
	public VoteService voteService;

	@ExceptionHandler({ BusinessException.class })
	public ResponseEntity<?> handleSicrediBusinessException(
			BusinessException businessException) {
		final ResponseOutput<?> response = new ResponseOutput<Serializable>(
				businessException.getMessage());

		return ResponseEntity.status(businessException.getHttpStatus()).body(
				response);
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<?> handleException(Exception e) {
		final ResponseOutput<?> response = new ResponseOutput<Serializable>(
				e.getMessage());

		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
				response);
	}

	ResponseEntity<?> errorsResponse(BindingResult result) {
		final ResponseOutput<Serializable> response = new ResponseOutput<Serializable>(
				result.getAllErrors().stream()
						.map(ObjectError::getDefaultMessage)
						.collect(Collectors.toList()));

		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(
				response);
	}

}

package br.com.sicredi.api;

import java.time.ZoneOffset;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.sicredi.client.DiscussionIn;
import br.com.sicredi.client.ResponseOutput;
import br.com.sicredi.client.SessionIn;
import br.com.sicredi.entity.dto.DiscussionDTO;
import br.com.sicredi.entity.dto.KeyDTO;
import br.com.sicredi.entity.dto.SessionDTO;
import br.com.sicredi.service.business.DiscussionServiceImpl;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/v1/discussions")
public class DiscussionApi extends ApiBase {

	private static final long serialVersionUID = 1L;

	@Autowired
	private DiscussionServiceImpl discussionService;

	@Autowired
	private ModelMapper modelMapper;

	@ApiOperation(value = "Criação nova discussão.")
	@PostMapping(path = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createDiscussion(
			@RequestBody @Valid DiscussionIn discussion, BindingResult result) {

		if (result.hasErrors()) {
			return errorsResponse(result);
		}

		final ResponseOutput<KeyDTO> response = new ResponseOutput<KeyDTO>(
				discussionService.createDiscussion(modelMapper.map(discussion,
						DiscussionDTO.class)));
		logger.info(StringUtils.join("createDiscussion method: ", response));

		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@ApiOperation(value = "Busca discussão.")
	@GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findDiscussion(@PathVariable Long id) {

		final ResponseOutput<DiscussionDTO> response = new ResponseOutput<DiscussionDTO>(
				discussionService.findDiscussion(id));
		logger.info(StringUtils.join("findDiscussion method ", response));

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	@ApiOperation(value = "Nova sessão de votação. ")
	@PostMapping(path = "/{discussionId}/sessions", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createSession(@PathVariable Long discussionId, @RequestBody @Valid SessionIn session, BindingResult result) {

		if (result.hasErrors()) {
			return errorsResponse(result);
		}

		final SessionDTO sessionDto = new SessionDTO();
		final Long deadline = session.getDeadline() == null ? null : session.getDeadline().atZone(ZoneOffset.systemDefault()).toInstant().toEpochMilli();
		sessionDto.setDeadline(deadline);
		final ResponseOutput<KeyDTO> response = new ResponseOutput<KeyDTO>(sessionService.createSession(discussionId, sessionDto));

		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}

	@ApiOperation(value = "Realiza a busca de sessão de votação.")
	@GetMapping(path = "/{discussionId}/sessions", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findSession(@PathVariable Long discussionId) {
		logger.info(StringUtils.join("createSession method ", discussionId));

		final ResponseOutput<SessionDTO> response = new ResponseOutput<SessionDTO>(sessionService.findSessionSummaryByDiscussionId(discussionId));
		logger.info(StringUtils.join("createSession method ", response));

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

}

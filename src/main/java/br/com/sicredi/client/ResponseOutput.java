package br.com.sicredi.client;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import br.com.sicredi.util.JsonUtils;

@JsonInclude(value = Include.NON_NULL)
public class ResponseOutput<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private T data;
	private String message;
	private List<String> errors;

	public ResponseOutput() {
		super();
	}

	public ResponseOutput(T data, String message, List<String> errors) {
		this();
		this.data = data;
		this.message = message;
		this.errors = errors;
	}

	public ResponseOutput(T data) {
		this();
		this.data = data;
	}

	public ResponseOutput(String message) {
		this();
		this.message = message;
	}

	public ResponseOutput(List<String> errors) {
		this();
		this.errors = errors;
	}

	public ResponseOutput(T data, String message) {
		this();
		this.data = data;
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return JsonUtils.logJson(this);
	}

}

package br.com.sicredi.client;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import br.com.sicredi.util.BusinessException;

@Service
public class ClientID {

	private static final String URI = "https://user-info.herokuapp.com/users/";
	@Autowired
	private Logger logger;

	@Autowired
	private RestTemplate restTemplate;

	public IDOutput permissionVerifier(String cpf) {
		logger.info(StringUtils.join("checkCpfPermission method ", cpf));

		final ResponseEntity<IDOutput> response;
		try {
			final String uri = StringUtils.join(URI, cpf);
			response = restTemplate.getForEntity(uri, IDOutput.class);

			final IDOutput cpfOutput = response.getBody();

			return cpfOutput;
		} catch (HttpClientErrorException | HttpServerErrorException e) {

			throw new BusinessException(e.getStatusCode(), e.getResponseBodyAsString());
		}
	}
}

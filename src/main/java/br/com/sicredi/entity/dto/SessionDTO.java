package br.com.sicredi.entity.dto;

import java.io.Serializable;

public class SessionDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private Long discussionId;
	private Long deadline;
	private Long created;
	private Boolean open;
	private SessionResultDTO sessionResult;

	public SessionDTO() {
		super();
	}

	public SessionDTO(Long id, Long discussionId, Long deadline, Long created, Boolean open, SessionResultDTO sessionResult) {
		this();
		this.id = id;
		this.discussionId = discussionId;
		this.deadline = deadline;
		this.created = created;
		this.open = open;
		this.sessionResult = sessionResult;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDiscussionId() {
		return discussionId;
	}

	public void setDiscussionId(Long discussionId) {
		this.discussionId = discussionId;
	}

	public Long getDeadline() {
		return deadline;
	}

	public void setDeadline(Long deadline) {
		this.deadline = deadline;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Boolean getOpen() {
		return open;
	}

	public void setOpen(Boolean open) {
		this.open = open;
	}

	public SessionResultDTO getSessionResult() {
		return sessionResult;
	}

	public void setSessionResult(SessionResultDTO sessionResult) {
		this.sessionResult = sessionResult;
	}


}
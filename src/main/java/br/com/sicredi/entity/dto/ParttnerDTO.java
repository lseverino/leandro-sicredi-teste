package br.com.sicredi.entity.dto;

import java.io.Serializable;

public class ParttnerDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String cpf;

	public ParttnerDTO() {
		super();
	}

	public ParttnerDTO(Long id, String cpf) {
		super();
		this.id = id;
		this.cpf = cpf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


}
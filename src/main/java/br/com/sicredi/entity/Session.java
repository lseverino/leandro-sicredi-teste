package br.com.sicredi.entity;

import java.io.Serializable;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash("Session")
public class Session implements Serializable {

	private static final long serialVersionUID = 1L;

	@Indexed
	private Long id;

	@Indexed
	private Long discussionId;
	private Long deadline;
	private Long created;
	
	@Indexed
	private Boolean open;

	public Session() {
		super();
	}

	public Session(Long id, Long discussionId, Long deadline, Long created, Boolean open) {
		super();
		this.id = id;
		this.discussionId = discussionId;
		this.deadline = deadline;
		this.created = created;
		this.open = open;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDiscussionId() {
		return discussionId;
	}

	public void setDiscussionId(Long discussionId) {
		this.discussionId = discussionId;
	}

	public Long getDeadline() {
		return deadline;
	}

	public void setDeadline(Long deadline) {
		this.deadline = deadline;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Boolean getOpen() {
		return open;
	}

	public void setOpen(Boolean open) {
		this.open = open;
	}

	@Override
	public String toString() {
		return "Session [id=" + id + ", discussionId=" + discussionId + ", deadline=" + deadline + ", created="
				+ created + ", open=" + open + "]";
	}

}

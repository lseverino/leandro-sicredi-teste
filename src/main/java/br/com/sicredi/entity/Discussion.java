package br.com.sicredi.entity;

import java.io.Serializable;
import org.springframework.data.redis.core.index.Indexed;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("Discussion")
public class Discussion implements Serializable {

	private static final long serialVersionUID = 1L;

	@Indexed
	private Long id;

	@Indexed
	private String title;

	@Indexed
	private String description;

	@Indexed
	private Long created;

	public Discussion() {
		super();
	}

	public Discussion(Long id, String title, String description, Long created) {
		this();
		this.id = id;
		this.title = title;
		this.description = description;
		this.created = created;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}


}
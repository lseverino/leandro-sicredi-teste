package br.com.sicredi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.sicredi.entity.Discussion;

@Repository
public interface DiscussionRepository extends CrudRepository<Discussion, Long> {


}

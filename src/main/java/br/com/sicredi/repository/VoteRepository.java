package br.com.sicredi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.sicredi.entity.Vote;

@Repository
public interface VoteRepository extends CrudRepository<Vote, Long> {

	@Cacheable
	Optional<Vote> findBySessionIdAndPartnerId(Long sessionId, Long partnerId);

	List<Vote> findBySessionId(Long sessionId);


}	
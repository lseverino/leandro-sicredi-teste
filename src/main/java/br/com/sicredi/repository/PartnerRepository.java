package br.com.sicredi.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.sicredi.entity.Parttner;

@Repository
public interface PartnerRepository extends CrudRepository<Parttner, Long> {

	Optional<Parttner> findByIdentity(String cpf);


}

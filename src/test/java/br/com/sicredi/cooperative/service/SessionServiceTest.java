package br.com.sicredi.cooperative.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.sicredi.entity.Discussion;
import br.com.sicredi.entity.Session;
import br.com.sicredi.entity.dto.SessionDTO;
import br.com.sicredi.repository.DiscussionRepository;
import br.com.sicredi.repository.SessionRepository;
import br.com.sicredi.service.business.SessionServiceImpl;
import br.com.sicredi.util.BusinessException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SessionServiceTest {

	@MockBean
	private SessionRepository sessionRepository;

	@MockBean
	private DiscussionRepository discussionRepository;

	@Autowired
	private SessionServiceImpl sessionService;

	@Test
	public void createSessionOkTest() {
		when(discussionRepository.findById(eq(1L))).thenReturn(Optional.of(new Discussion(1L, "Testing", "Testing ok", 1L)));

		sessionService.createSession(1L, new SessionDTO(1L, 1L, 12345679L, 12345679L, Boolean.TRUE, null));

		verify(discussionRepository, times(1)).findById(eq(1L));
		verify(sessionRepository, times(1)).save(any(Session.class));
	}

	@Test
	public void createSessionNotFoundTest() {
		when(discussionRepository.findById(eq(1L))).thenReturn(Optional.empty());

		try {
			sessionService.createSession(1L, new SessionDTO(1L, 1L, 12345679L, 12345679L, Boolean.TRUE, null));
		} catch (BusinessException sbe) {
			verify(discussionRepository, times(1)).findById(eq(1L));
			verify(sessionRepository, times(0)).save(any(Session.class));
			assertEquals(HttpStatus.NOT_FOUND, sbe.getHttpStatus());
		}
	}

	@Test
	public void createSessionConflictTest() {
		when(sessionRepository.findById(eq(1L))).thenReturn(Optional.of(new Session()));
		when(discussionRepository.findById(eq(1L))).thenReturn(Optional.of(new Discussion(1L, "Testing", "Testing ok", 1L)));

		try {
			sessionService.createSession(1L, new SessionDTO(1L, 1L, 12345679L, 12345679L, Boolean.TRUE, null));
		} catch (BusinessException sbe) {
			verify(discussionRepository, times(1)).findById(eq(1L));
			verify(sessionRepository, times(0)).save(any(Session.class));
			assertEquals(HttpStatus.CONFLICT, sbe.getHttpStatus());
		}
	}
}

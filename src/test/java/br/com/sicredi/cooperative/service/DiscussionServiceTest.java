package br.com.sicredi.cooperative.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.sicredi.entity.Discussion;
import br.com.sicredi.entity.dto.DiscussionDTO;
import br.com.sicredi.repository.DiscussionRepository;
import br.com.sicredi.service.business.DiscussionServiceImpl;
import br.com.sicredi.util.BusinessException;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class DiscussionServiceTest {

	@MockBean
	private DiscussionRepository discussionRepository;

	@Autowired
	private DiscussionServiceImpl discussionService;

	private Discussion discussionMock;

	@Before
	public void setUp() {
		discussionMock = new Discussion(1L, "Testing", "Testing ok", 1L);
		when(discussionRepository.findById(eq(1L))).thenReturn(Optional.of(discussionMock));
		when(discussionRepository.save(any(Discussion.class))).thenReturn(discussionMock);
	}

	@Test
	public void findDiscussionOkTest() {
		final DiscussionDTO discussionDto = discussionService.findDiscussion(1L);

		assertEquals(discussionDto.getId(), discussionMock.getId());
		assertEquals(discussionDto.getTitle(), discussionMock.getTitle());
		assertEquals(discussionDto.getDescription(), discussionMock.getDescription());

		verify(discussionRepository, times(1)).findById(1L);
	}

	@Test
	public void findDiscussionNotFoundTest() {
		try {
			discussionService.findDiscussion(987654321L);
		} catch (BusinessException sbe) {
			verify(discussionRepository, times(1)).findById(987654321L);
			verify(discussionRepository, times(0)).save(any(Discussion.class));
			assertEquals(HttpStatus.NOT_FOUND, sbe.getHttpStatus());
		}
	}

	@Test
	public void createDiscussionOkTest() {
		final DiscussionDTO discussionDto = new DiscussionDTO(1L, "Test", "ABECE", 2L);
		discussionService.createDiscussion(discussionDto);

		verify(discussionRepository, times(1)).save(any(Discussion.class));
	}


}

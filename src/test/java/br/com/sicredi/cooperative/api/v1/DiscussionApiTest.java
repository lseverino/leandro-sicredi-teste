package br.com.sicredi.cooperative.api.v1;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.eq;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.sicredi.client.DiscussionIn;
import br.com.sicredi.entity.Discussion;
import br.com.sicredi.repository.DiscussionRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class DiscussionApiTest {

	@MockBean
	private DiscussionRepository discussionRepository;

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void createDiscussionOkTest() throws Exception {
		when(discussionRepository.save(any(Discussion.class))).thenReturn(new Discussion(1L, "Testing", "Testing ok", 1L));

		mvc.perform(MockMvcRequestBuilders.post("/v1/discussions/")
				.content(objectMapper.writeValueAsString(new DiscussionIn("Test", "Test ok case", null)))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());

	}

	@Test
	public void createDiscussionUnprocessableEntityTest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/v1/discussions/")
				.content(objectMapper.writeValueAsString(new DiscussionIn(null, null, null)))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isUnprocessableEntity());

	}

	@Test
	public void findDiscussionOkTest() throws Exception {
		when(discussionRepository.findById(eq(1L))).thenReturn(Optional.of(new Discussion()));

		mvc.perform(MockMvcRequestBuilders.get("/v1/discussions/1")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());

	}

	@Test
	public void findDiscussionNotFoundTest() throws Exception {
		when(discussionRepository.findById(eq(1L))).thenReturn(Optional.empty());

		mvc.perform(MockMvcRequestBuilders.get("/v1/discussions/1")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());

	}


}
